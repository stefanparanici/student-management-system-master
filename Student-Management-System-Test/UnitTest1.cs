using NUnit.Framework;
using System.Collections.Generic;
using Student_Management_System.Controllers;
using Student_Management_System.DAL;
using Student_Management_System.Models;

namespace Student_Management_System_Test
{
    public class Tests
    {
        private SchoolContext _schoolContext;
        private StudentController _studentController;

        [SetUp]
        public void Setup()
        {
            _schoolContext = new SchoolContext();
        }

        [Test]
        public void FirstName_should_under_64_charcters()
        {
            //Given
            _studentController = new StudentController(_schoolContext);

            //Then
            List<Student> allStudents = _studentController.GetAll();

            //When
            foreach (var result in allStudents)
            {
                Assert.That(result.FirstName.Length, Is.LessThan(64));
            }
        }
        [Test]
        public void LastName_should_under_64_charcters()
        {
            //Given
            _studentController = new StudentController(_schoolContext);

            //Then
            List<Student> allStudents = _studentController.GetAll();

            //When
            foreach (var result in allStudents)
            {
                Assert.That(result.LastName.Length, Is.LessThan(64));
            }
        }
        [Test]
        public void FirstName_should_notBe_null()
        {
            //Given
            _studentController = new StudentController(_schoolContext);

            //Then
            List<Student> allStudents = _studentController.GetAll();

            //When
            foreach (var result in allStudents)
            {
                Assert.That(result.FirstName!="", Is.True);
            }
        }
        [Test]
        public void LastName_should_notBe_null()
        {
            //Given
            _studentController = new StudentController(_schoolContext);

            //Then
            List<Student> allStudents = _studentController.GetAll();

            //When
            foreach (var result in allStudents)
            {
                Assert.That(result.LastName != "", Is.True);
            }
        }
        [Test]
        public void Id_should_notBe_null()
        {
            //Given
            _studentController = new StudentController(_schoolContext);

            //Then
            List<Student> allStudents = _studentController.GetAll();

            //When
            foreach (var result in allStudents)
            {
                Assert.That(result.Id != "", Is.True);
            }
        }

        [Test]
        public void no_available_student()
        {
            //Given
            string studentId = "d3d8e6fd-cc8e-41e7-94c3-8a9d2ae4e4b7";//StudentID invalid
            string courseId = "ad938a7d-ead9-4610-8252-d4ef4823f194";
            int grade = 7;

            //When 

            _studentController = new StudentController(_schoolContext);

            //Then
            Assert.That(() => _studentController.AddGrade(studentId, courseId, grade), !Throws.Exception.With.Message.Contain("Student not existing in database!"));
        }
        [Test]
        public void no_available_course()
        {
            //Given
            string studentId = "d3d8e6fd-cc8e-41e7-94c3-8b9d2ae4e4a7";
            string courseId = "a3277b72-e167-4dbd-ac3f-af39d9538c6d"; //CourseID invalid
            int grade = 7;

            //When 

            _studentController = new StudentController(_schoolContext);

            //Then
            Assert.That(() => _studentController.AddGrade(studentId, courseId, grade), !Throws.Exception.With.Message.Contain("Student not enrolled to course!"));
        }

        [Test]
        public void no_student_with_another_id()
        {
            //Given
            string _studentId = "d3d8e6fd-cc8e-41e7-94c3-8b9d2a345345";
            string _FirstName = "Gheorghe";
            string _LastName = "Vasile";

            //When

            Student x = new Student {FirstName = _FirstName, LastName = _LastName, Id = _studentId};

            //Then
            Assert.That(()=>_studentController.Update(x),Throws.Exception.With.Message.Contain("Operation not allowed!"));
        }
    }
}