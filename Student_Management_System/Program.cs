﻿using Student_Management_System.Controllers;
using Student_Management_System.DAL;

namespace Student_Management_System
{
    class Program
    {
        static void Main()
        {
            SchoolContext schoolContext = new SchoolContext();
            StudentController studentController = new StudentController(schoolContext);

            string studentId = "d3d8e6fd-cc8e-41e7-94c3-8s9d2de4g4b7";
            string courseId = "ad938a7d-ead9-4610-8252-d4ef4823f194";
            int grade = 7;

            studentController.AddGrade(studentId,courseId,grade);
        }
    }
}
